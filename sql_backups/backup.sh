#!/bin/bash
# backup all the databases from the list

#==============================================
# Variables
#==============================================
BASEDIR=$(dirname $0)
LIST=$BASEDIR"/databases.txt"
DEST=$BASEDIR"/backups/"
CURRENT_DATE=$(date +"%Y-%m-%d");

#==============================================
# Open and read file
#==============================================
echo "Opening databases file $LIST"
while IFS= read -r line
do
    IFS=',' read -r -a PARTS <<< "$line"
    SQL_FILE=$DEST"${PARTS[3]}"
    # export the table
    echo "exporting ${PARTS[3]}"
    $(mysqldump -h ${PARTS[0]} -u ${PARTS[1]} -p${PARTS[2]} ${PARTS[3]} > $SQL_FILE".sql")
    # compress
    echo "Compressing $SQL_FILE"
    tar zcf "$SQL_FILE""_""$CURRENT_DATE.tar.gz" "$SQL_FILE.sql"
    # delete
    rm "$SQL_FILE.sql"
done < $LIST

echo "done!"