#!/bin/bash
default=100
limit=${1:-$default}

for i in $(seq $limit)
do
	factors=( $(factor $i) )
	first=${factors[1]}
	if [ "$i" = "$first" ]
		then
		echo $i
	fi
done