#!/bin/bash
# /bin/bash script.sh

# parameter substitution
echo "-= parameters substitution =-"
echo -n "print without \n"
var1=1
var2=2
echo ${var1:-$var2}
echo ${var3:-$var2}
# variable length
echo "-= variable length =-"
string="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique tellus eu sapien mollis accumsan."
echo ${#string}
echo "-= number of positional parameters =-"
echo ${#*}
echo $#
# Substring Extraction
# ${string:position}
# ${string:position:length}
echo "-= substring =-"
echo ${string:5}
echo ${string:5:6}
# substitution
echo "-= substitutions =-"
file="/var/www/user.conf"
echo $file
file=${file%.conf}
echo $file
file=${file##*/}
echo $file
# ${string//find/replace}
echo ${string/Lorem ipsum/Yololo}
echo ${string//./[+]}
# arrays
echo "-= arrays =-"
arr=( 10 11 12 13 14 15 )
for i in ${arr[@]}
do
	if [ "$i" -lt 13 ]
		then
		echo $i
		continue
	else
		break
	fi
done

declare -a files=$(ls .)
for j in ${files[@]}
do
	echo $j
done

list=$(ls .)
for file in $list
do
	echo $file
done

# no subshells
echo "-= no subshell =-"
while read i
do
	echo $i
done < <( ls . )

# functions
# the variables inside a function are global, mark them as local for local variables
echo "-= functions =-"
test_function ()
{
	lorem="lorem"
	local ipsum="ipsum"
	echo $1 $lorem $ipsum
	echo "-= debug =-"
	caller 0 # debug who calls this function
}
test_function "asdf1234"
echo "global: $lorem"
echo "local: $ipsum"

# let
echo "-= let =-"
test=10
while [ "$test" -gt "0" ]
do
	echo $test
	let "test -= 1"
done

# debug
echo "-= debug =-"
set -x
echo $LINENO
set +x

# colorizing
echo "-= colorizing =-"
echo -e '\E[37;44m'"\033[1mContact List\033[0m" # White on blue background
echo -e "\033[1mThis is bold text.\033[0m" # bold
echo -e "\033[4mThis is underlined text.\033[0m" # underline
echo -e '\E[35m'"\033[1mMagenta\033[0m" # Magenta
echo -e '\E[32m'"\033[1mGreen\033[0m" # Green
echo -e '\E[31m'"\033[1mRed\033[0m" # Red
tput sgr0 # reset

# Numbers representing colors in Escape Sequences
# colors	Foreground	Background
# black		30			40
# red 		31 			41
# green 	32 			42
# yellow 	33 			43
# blue 		34 			44
# magenta 	35 			45
# cyan 		36 			46
# white 	37 			47

function str_pad {
	printf "| %-$2s |\n" "$1"
}