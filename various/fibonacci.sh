#!/bin/bash
default_limit=20
limit=${1:-$default_limit}

start=1
current=$start
prev=0
# first number
echo "0"
# sequence
for i in $(seq $limit)
do
	echo "$current"
	next=$(( current+prev ))
	prev=$current
	current=$next
done