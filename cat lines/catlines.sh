#!/bin/bash
file=$1
start=${2:-1}
default_end=$(( $start + 10 ))
end=${3:-$default_end}

# there's no file, so display help
if [ ! -f "$file" -o "$file" = "--help" ]; then
	echo "Usage: $0 filename start end"
	echo "catlines works like head or tail, but it could start on any line"
	echo "and end on any line"
	exit 0
fi
# check if end is greater than the start
if [ "$start" -gt "$end" ]; then
	end=$(( $start + 10 ))
fi
# print lines
count=1
while read line
do
	if [ "$count" -ge "$start" ]; then
		echo $line
	fi
	let "count += 1"
	if [ "$count" -ge "$end" ]; then
		break
	fi
done < $file