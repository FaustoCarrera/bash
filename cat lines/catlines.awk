#!/usr/bin/awk
# awk -f catlines.awk start=222000 end=232000 temp.txt
{
	if ((NR >= start) && (NR <= end)) {
		print $0
	}
	if (NR > end) {
		exit;
	}
}