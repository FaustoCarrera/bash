#!/bin/bash
# database data
db_host="localhost"
db_user="root"
# clear screen
clear
# project data
echo "Answer the questions and hit enter key."
echo "project route:"
read route
echo "database:"
read database
echo "destination folder: (default is $HOME/backups)"
read destination

# destination
if [ ! "$destination" ]; then
	destination="$HOME/backups"
fi

# check project route
if [ ! "$route" ]; then
	echo "Error: You has to specify the project route"
	exit 1
elif [ ! -d "$route" ]; then
	echo "Error: The project route is invalid, $route does not exists"
	exit 1
else
	project=${route##*/}
fi

# copy project to backup folder
if [ ! -d "$destination" ]; then
	mkdir -p $destination
fi

if [ -d "$destination/$project" ]; then
	rm -rf $destination"/"$project
fi

du=$(du -s -h $route)
echo "moving "$du
cp -r $route $destination"/"$project

# backup database
if [ "$database" ]; then
	sql_folder=$destination"/"$project"/_sql"
	mkdir -p $sql_folder
	echo "Backing up database"
	result=$(mysqldump -h $db_host -u $db_user -p $database > $sql_folder"/"$database".sql")
fi

# tar files
cd $destination
tar_project=$project".tar.gz"
if [ -f $tar_project ]; then
	unlink $tar_project
fi
echo "backing up files..."
tar zcf $tar_project $project
rm -rf $project

echo "done!"

#echo "$project $route $database $destination $backup_folder"