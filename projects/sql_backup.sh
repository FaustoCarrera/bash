#!/bin/sh
#+----------------------------------------
# Export all the current databases
# usage: sql_backup.sh
#+----------------------------------------

echo "destination folder: (default is $HOME/backups)"
read destination

# destination
if [ ! "$destination" ]; then
	destination="$HOME/backups"
fi

if [ ! -d "$destination" ]; then
	echo "Error: The route is invalid, $destination does not exists"
	exit 1
fi
# date variables
current_date=$(date +"%Y-%m-%d");
# database data
db_host="localhost"
db_user="root"
omited="information_schema,cdcol,mysql,performance_schema,phpmyadmin"
sql_folder=$destination"/sql"
tmp=$sql_folder"/tmp"
# clear screen
clear
# check if destination exists
if [ ! -d "$sql_folder" ]; then
	mkdir -p $sql_folder
fi
# query the mysql database for the list of databases
echo "Backing up databases"
echo "database password:"
read db_pass
query_dblist=$(echo 'SHOW DATABASES;' | mysql -h $db_host -u $db_user -p$db_pass -s > $tmp);
databases=$(cat $tmp; unlink $tmp)
for db in $databases
do
	if echo $omited | grep -q -v "$db"; then
		sql_file=$sql_folder"/"$db".sql"
		if [ -f "$sql_file" ]
		then
			$(unlink $sql_file)
		fi
		echo "exporting $db"
		$(mysqldump -h localhost -u $db_user -p$db_pass $db > $sql_file)
	fi
done

# backup files
cd $destination
tar zcf "sql_"$current_date".tar.gz" "sql"
rm -rf $sql_folder

echo "done!"