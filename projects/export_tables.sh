#!bin/bash
# export each table to an independent sql file
# you have to provide a file with the tables names
# the database, user and password

FILE=$1
HOST=$2
PORT=$3
DATABASE=$4
USER=$5
PASS=$6

# check the parameters
if [ $# -eq 0 ]; then
    echo "Usage: /bin/bash $0 FILENAME HOST PORT DATABASE USER PASSWORD"
    exit 1
fi

# check if file exists
if [ ! -f "$FILE" ]; then
    echo "Error: The file is invalid, $FILE does not exists"
    exit 1
fi

while read TABLE
do
    echo "Exporting table $TABLE"
    $(mysqldump --no-data --host="$HOST" --port="$PORT" --user="$USER" --password="$PASS" "$DATABASE" "$TABLE" > "$TABLE.sql")
done < $FILE