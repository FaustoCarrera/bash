#!/bin/bash
# This script make a backup of the call logs and the consumers call logs
# Files:
# /var/www/billing/v1/cli_cron/call_logs/logs
# Process:
# make a folder called with the current date/time inside call_logs
# make a folder called call_logs inside

action=$1
verify=1

#==============================================
# Variables
#==============================================
# date/time variables
current_day=$(date +"%Y-%m-%d");
current_time=$(date +"%H-%M-00");
# folders
base="./logs"
#base="/var/www/newapp/log"
call_logs=$base"/call_logs"
logs=$call_logs"/logs"
old_logs=$call_logs"/old_logs"

#==============================================
# you have to run it as root
#==============================================
if [ $(whoami) != "root" ]
then
	echo "============================================================="
	echo "-~-~-~   Error: the logs script must be run as root    ~-~-~-"
	echo "============================================================="
	exit 1
fi

#==============================================
# Actions
#==============================================

if [ -z "$action" ]
	then
	echo "========================================================================="
	echo "Usage       : /bin/bash $0 [options]"
	echo -e "\033[1mOptions:\033[0m" # bold
	echo "size        : checks the size of the folder"
	echo "ls          : checks the content of the folder"
	echo "backup      : check the size of the folder and back it up if size > 100Mb"
	echo "backup-force: force backup no matter the size"
	echo "interactive : for an interactive screen"
	echo "========================================================================="
	exit 0
fi

if [ "$action" = "watch" ]
	then
	while :
	do
		echo "==================================================================="
		result=$(du -sh $logs)
		echo $result
		echo "==================================================================="
		sleep 1
		clear
	done
fi

if [ "$action" = "size" ]
	then
	echo "==================================================================="
	result=$(du -sh $logs)
	echo $result
	echo "==================================================================="
fi

if [ "$action" = "ls" ]
	then
	echo "==================================================================="
	result=$(ls -A $logs)
	for file in $result
	do
		du -sh "$logs/$file"
	done
	count="$(find $logs -mindepth 1 -maxdepth 1 | wc -l)"
	echo "Total $count files"
	echo "==================================================================="
fi

if [ "$action" = "backup-force" ]
	then
	action="backup"
	verify=0
fi

if [ "$action" = "backup" ]
	then
	go=1
	# check if we have enough log to move
	size=$(du -s $logs | awk '{ print $1 }');
	limit=102400
	if [ "$verify" = "1" ]
		then
		if [ "$size" -lt "$limit" ]
			then
			echo "==================================================================="
			echo "Sorry, not enough logs"
			echo "==================================================================="
			go=0
		fi
	fi

	if [ "$go" = "1" ]
		then
		# create destination folder
		destination=$old_logs"/"$current_day
		if [ ! -d "$destination" ]
			then
			mkdir -p $destination
		fi

		# create the log folder
		log_folder=$destination"/"$current_time
		if [ -d "$log_folder" ]
			then
			rm -rf $log_folder
		fi
		mkdir $log_folder

		# move files to backup folder
		files=$(ls $logs | grep -v consumer)
		for file in $files
		do
			mv $logs"/"$file $log_folder"/"$file
		done

		# count moved files
		count="$(find $log_folder -mindepth 1 -maxdepth 1 | wc -l)"

		# generate the backup
		cd $destination
		if [ "$count" = "0" ]
			then
			# delete log folder
			rm -rf $current_time
		else
			tar_file=$current_time".tar.gz"
			# check if backup exists
			if [ -f $tar_file ]
				then
				unlink $tar_file
			fi
			# backup files
			tar zcf $tar_file $current_time
			rm -rf $current_time
		fi

		echo "New backup generated $destination/$tar_file"
	fi
fi

#==============================================
# interactive
#==============================================

if [ "$action" = "interactive" ]
	then
	echo "==================================================================="
	echo -e "\033[1mOptions:\033[0m" # bold
	echo "size        : checks the size of the folder"
	echo "backup      : check the size of the folder and back it up if size > 100Mb"
	echo "backup-force: force backup no matter the size"
	echo "==================================================================="
	echo "Enter option:"
	while read data
	do
		action=$data
		if [ "$action" != "interactive" ]
			then
			/bin/bash ./$0 $action
		fi
	done
fi